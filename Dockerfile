# Bluesky Base Dockerfile

##### shared environment stage #################################################

# Use Ubuntu 20.04 as the base image.
FROM python:3.10-slim-bullseye
ENV TZ=Europe/Berlin

##### developer / build stage ##################################################

# Install essential build tools and utilities.
RUN apt update -y && apt upgrade -y
RUN apt install -y --no-install-recommends \
    git \
    libgl1-mesa-dri \
    libgl1-mesa-dri \
    tzdata \
    re2c \
    rsync \
    ssh-client \
    nano

# Set the timezone to 'Europe/Berlin'.    
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Install the required system packages
RUN apt install -y libgl-dev libxkbcommon-dev libegl-dev libfontconfig-dev libdbus-1-dev \
    libxcb-*-dev libxkbcommon-x11-dev    
RUN apt install -y libzbar-dev

# Install the required python packages
RUN pip install --upgrade pip
COPY requirements.txt .
RUN pip install -r requirements.txt

# Create directory structure for Bluesky
RUN mkdir /opt/bluesky
# Clone the 'beamline_test' Git repository into the 'bluesky' directory.
RUN cd /opt/bluesky && git clone --branch v1.4.3 https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii.git
# bessy devices
RUN cd /opt/bluesky && git clone --branch v1.4.1 https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii_devices.git
#bluesky_hooks
RUN cd /opt/bluesky && git clone --branch 0-0-1 https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bluesky_hooks.git
#suitcase-specfile
RUN cd /opt/bluesky && git clone --branch main https://codebase.helmholtz.cloud/hzb/bluesky/suitcase-specfile.git
#bluesky device instantiation
RUN cd /opt/bluesky && git clone --branch main https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii_devices_instantiation.git
#install apstools without databroker dependency
RUN pip install apstools@git+https://codebase.helmholtz.cloud/hzb/bluesky/core/source/apstools.git

# Install the packages downloaded in bluesky
RUN cd /opt/bluesky/bessyii && pip install .
RUN cd /opt/bluesky/bessyii_devices && pip install .
RUN cd /opt/bluesky/bluesky_hooks && pip install .
RUN cd /opt/bluesky/suitcase-specfile && pip install .
RUN cd /opt/bluesky/bessyii_devices_instantiation && pip install .

# Clone the 'shell-scripts-container' Git repository into the '/opt' directory.
RUN cd /opt && git clone https://codebase.helmholtz.cloud/hzb/bluesky/core/source/shell-scripts-for-container.git

# Set the PATH environment variable to include '/opt/shell-scripts-container/bin'.
ENV PATH="/opt/shell-scripts-for-container/bin:$PATH"

# Create a directory structure and clone 'profile_root' Git repository.
RUN mkdir -p /opt/bluesky/ipython && cd /opt/bluesky/ipython

# Copy the startup files to the container
COPY startup/ /opt/bluesky/ipython/profile_root/startup

# Set the working directory to '/bluesky/'.
WORKDIR /opt/bluesky/

# Add ipython_config.py to the container.
COPY ipython_config.py /opt/bluesky/ipython/profile_root/ipython_config.py


