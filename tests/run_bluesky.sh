#!/bin/bash

# Display usage instructions
show_help() {
    echo "Usage: ./script.sh [sh | down]"
    echo "Start the container as root"
    echo "  sh: Optional command to enter container"
    echo "  stop: Stop and remove the container"
}

# Check for help option
if [[ "$1" == "-h" || "$1" == "--h" || "$1" == "-help" || "$1" == "--help" || "$1" == "help" || "$1" == "h" ]]; then
    show_help
    exit 0
fi


# Check if running in a CI environment
if [ -z "$CI" ]; then
  echo "Not in a CI environment, setting up xhost"
  if ! command -v xhost &> /dev/null; then
    echo "xhost could not be found, exiting"
    exit 1
  fi

  xhost +local:
fi

# Ensure Docker is available
if ! command -v docker &> /dev/null; then
  echo "Docker could not be found, exiting"
  exit 1
fi

# Define the container name
target_container="bluesky"

deploy_container() {
    # Check if we're running in a CI environment
    if [ "$CI" = "true" ]; then
        if [ "$CI_COMMIT_REF_NAME" = "develop" ]; then
            echo "Running in CI environment: Skipping localtime and timezone mounts"
            docker run --name ${target_container} \
            --env DISPLAY=${DISPLAY} \
            --env TILED_URL=${TILED_URL} \
            --env TILED_API_KEY=${TILED_API_KEY} \
            --env _DEV_CFG_FILE_DIR_PATH=${_DEV_CFG_FILE_DIR_PATH} \
            --env _RML_FILE_DIR_PATH=${_RML_FILE_DIR_PATH} \
            --env _HAPPI_DB_FILE_DIR_PATH=${_HAPPI_DB_FILE_DIR_PATH} \
            --env _DEV_INSTANTIATION_SETUP_FILE_DIR_PATH=${_DEV_INSTANTIATION_SETUP_FILE_DIR_PATH} \
            --env DEV_INSTANTIATION_SETUP_FILE_NAME=${DEV_INSTANTIATION_SETUP_FILE_NAME} \
            --network host \
            -v /tmp/.X11-unix:/tmp/.X11-unix \
            -v /builds/hzb/bluesky/core/images/bluesky_container/beamlinetools:/opt/bluesky/beamlinetools \
            -v /builds/hzb/bluesky/core/images/bluesky_container/bluesky/data:/opt/bluesky/data \
            -v /builds/hzb/bluesky/core/images/bluesky_container/bluesky/user_scripts:/opt/bluesky/user_scripts \
            -v ${DEV_CFG_FILE_DIR_PATH}:${_DEV_CFG_FILE_DIR_PATH} \
            -v ${RML_FILE_DIR_PATH}:${_RML_FILE_DIR_PATH} \
            -v ${HAPPI_DB_FILE_DIR_PATH}:${_HAPPI_DB_FILE_DIR_PATH} \
            -v ${DEV_INSTANTIATION_SETUP_FILE_DIR_PATH}:${_DEV_INSTANTIATION_SETUP_FILE_DIR_PATH} \
            $CI_REGISTRY_IMAGE:latest \
            sh -c "python3 -m pip install -e /opt/bluesky/beamlinetools && pytest --junit-xml=/opt/bluesky/data/report.xml /opt/bluesky/beamlinetools/tests && ipython --ipython-dir=/opt/bluesky/ipython --profile=root -c ' from ophyd.sim import det1, motor; print(det1, motor); from bluesky.plans import count; RE(count([det1]))'"
        
        elif [ -n "$CI_COMMIT_TAG" ]; then

            echo "Running in CI environment: Skipping localtime and timezone mounts"
            docker run --name ${target_container} \
            --env DISPLAY=${DISPLAY} \
            --env TILED_URL=${TILED_URL} \
            --env TILED_API_KEY=${TILED_API_KEY} \
            --env _DEV_CFG_FILE_DIR_PATH=${_DEV_CFG_FILE_DIR_PATH} \
            --env _RML_FILE_DIR_PATH=${_RML_FILE_DIR_PATH} \
            --env _HAPPI_DB_FILE_DIR_PATH=${_HAPPI_DB_FILE_DIR_PATH} \
            --env _DEV_INSTANTIATION_SETUP_FILE_DIR_PATH=${_DEV_INSTANTIATION_SETUP_FILE_DIR_PATH} \
            --env DEV_INSTANTIATION_SETUP_FILE_NAME=${DEV_INSTANTIATION_SETUP_FILE_NAME} \
            --network host \
            -v /tmp/.X11-unix:/tmp/.X11-unix \
            -v /builds/hzb/bluesky/core/images/bluesky_container/beamlinetools:/opt/bluesky/beamlinetools \
            -v /builds/hzb/bluesky/core/images/bluesky_container/bluesky/data:/opt/bluesky/data \
            -v /builds/hzb/bluesky/core/images/bluesky_container/bluesky/user_scripts:/opt/bluesky/user_scripts \
            -v ${DEV_CFG_FILE_DIR_PATH}:${_DEV_CFG_FILE_DIR_PATH} \
            -v ${RML_FILE_DIR_PATH}:${_RML_FILE_DIR_PATH} \
            -v ${HAPPI_DB_FILE_DIR_PATH}:${_HAPPI_DB_FILE_DIR_PATH} \
            -v ${DEV_INSTANTIATION_SETUP_FILE_DIR_PATH}:${_DEV_INSTANTIATION_SETUP_FILE_DIR_PATH} \
            $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG \
            sh -c "python3 -m pip install -e /opt/bluesky/beamlinetools && pytest --junit-xml=/opt/bluesky/data/report.xml /opt/bluesky/beamlinetools/tests && ipython --ipython-dir=/opt/bluesky/ipython --profile=root -c ' from ophyd.sim import det1, motor; print(det1, motor); from bluesky.plans import count; RE(count([det1]))'"
        
        elif [ -z "$CI_COMMIT_TAG" ] && [ "$CI_COMMIT_REF_NAME" != "develop" ] && [ "$CI_COMMIT_REF_NAME" != "main" ]; then
            echo "Running in CI environment: Skipping localtime and timezone mounts"
            docker run --name ${target_container} \
            --env DISPLAY=${DISPLAY} \
            --env TILED_URL=${TILED_URL} \
            --env TILED_API_KEY=${TILED_API_KEY} \
            --env _DEV_CFG_FILE_DIR_PATH=${_DEV_CFG_FILE_DIR_PATH} \
            --env _RML_FILE_DIR_PATH=${_RML_FILE_DIR_PATH} \
            --env _HAPPI_DB_FILE_DIR_PATH=${_HAPPI_DB_FILE_DIR_PATH} \
            --env _DEV_INSTANTIATION_SETUP_FILE_DIR_PATH=${_DEV_INSTANTIATION_SETUP_FILE_DIR_PATH} \
            --env DEV_INSTANTIATION_SETUP_FILE_NAME=${DEV_INSTANTIATION_SETUP_FILE_NAME} \
            --network host \
            -v /tmp/.X11-unix:/tmp/.X11-unix \
            -v /builds/hzb/bluesky/core/images/bluesky_container/beamlinetools:/opt/bluesky/beamlinetools \
            -v /builds/hzb/bluesky/core/images/bluesky_container/bluesky/data:/opt/bluesky/data \
            -v /builds/hzb/bluesky/core/images/bluesky_container/bluesky/user_scripts:/opt/bluesky/user_scripts \
            -v ${DEV_CFG_FILE_DIR_PATH}:${_DEV_CFG_FILE_DIR_PATH} \
            -v ${RML_FILE_DIR_PATH}:${_RML_FILE_DIR_PATH} \
            -v ${HAPPI_DB_FILE_DIR_PATH}:${_HAPPI_DB_FILE_DIR_PATH} \
            -v ${DEV_INSTANTIATION_SETUP_FILE_DIR_PATH}:${_DEV_INSTANTIATION_SETUP_FILE_DIR_PATH} \
            $CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH \
            sh -c "python3 -m pip install -e /opt/bluesky/beamlinetools && pytest --junit-xml=/opt/bluesky/data/report.xml /opt/bluesky/beamlinetools/tests && ipython --ipython-dir=/opt/bluesky/ipython --profile=root -c ' from ophyd.sim import det1, motor; print(det1, motor); from bluesky.plans import count; RE(count([det1]))'"
        

        else
            echo "Skipping deployment: not a new tag and not the main branch."
        fi
    else
        echo "Not running in CI environment: Proceeding with local deployment"
        # Your local deployment commands here
    fi
}


# Cleanup function
cleanup() {
    echo "Removing container $target_container and exiting..."
    docker container stop "$target_container" && docker container rm "$target_container"
    # Add further cleanup steps if necessary
    exit 1
}

# Trap Ctrl+C and call the cleanup function
trap cleanup EXIT

command_to_run="$1"


# Check command arguments and execute corresponding actions
case "$command_to_run" in
    "stop")
        echo "Stopping and removing bluesky"
        docker container stop "$target_container" && docker container rm "$target_container"
        ;;
    "sh")
        deploy_container sh
        ;;
    *)
        # If no command specified, deploy the container
        deploy_container bluesky_start_root
        ;;
esac


