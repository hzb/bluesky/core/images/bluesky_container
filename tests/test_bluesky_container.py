# test_bluesky_container.py

import pytest
from ophyd.sim import det1, det2, det3, det4, motor1, motor2, motor3
from bluesky.plans import count

# Ensure the devices are connected
@pytest.fixture(scope="module")
def setup_devices():
    # This setup function runs once per module
    assert det1.connected, "det1 is not connected"
    assert det2.connected, "det2 is not connected"
    assert det3.connected, "det3 is not connected"
    assert det4.connected, "det4 is not connected"
    assert motor1.connected, "motor1 is not connected"
    assert motor2.connected, "motor2 is not connected"
    assert motor3.connected, "motor3 is not connected"
    print("All devices connected")

def test_count_plan(setup_devices):
    # Test that the count plan works
    try:
        # Run the count plan
        from bluesky import RunEngine
        RE = RunEngine({})
        RE(count([det1]))
        print("Count plan executed successfully")
    except Exception as e:
        pytest.fail(f"Count plan failed with exception: {e}")

