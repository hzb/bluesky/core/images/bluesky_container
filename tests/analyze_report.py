import xmltodict
import sys

def analyze_report():
    try:
        with open('report.xml') as file:
            report = xmltodict.parse(file.read())
    except FileNotFoundError:
        print("Error: report.xml not found.")
        sys.exit(1)
    except Exception as e:
        print(f"Error parsing report.xml: {e}")
        sys.exit(1)

    # Debugging: Print the structure of the report
    print("Parsed report structure:")
    print(report)

    # Check if 'testsuites' and 'testsuite' are in the report
    if 'testsuites' not in report or 'testsuite' not in report['testsuites']:
        print("Error: 'testsuites' or 'testsuite' key not found in the report.")
        sys.exit(1)

    testsuite = report['testsuites']['testsuite']
    
    try:
        tests = int(testsuite['@tests'])
        errors = int(testsuite['@errors'])
        failures = int(testsuite['@failures'])
        skipped = int(testsuite['@skipped'])
    except KeyError as e:
        print(f"Error: Missing key in the 'testsuite': {e}")
        sys.exit(1)

    print(f"Total tests: {tests}")
    print(f"Errors: {errors}")
    print(f"Failures: {failures}")
    print(f"Skipped: {skipped}")

    if errors > 0 or failures > 0:
        print("Some tests failed or encountered errors.")
        sys.exit(1)
    else:
        print("All tests passed successfully.")
        sys.exit(0)

if __name__ == "__main__":
    analyze_report()
