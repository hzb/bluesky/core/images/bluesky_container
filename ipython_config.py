c = get_config()

c.TerminalIPythonApp.display_banner = True
c.InteractiveShellApp.log_level = 20
c.InteractiveShell.autoindent = True
c.InteractiveShell.confirm_exit = False
c.InteractiveShell.editor = 'nano'
c.PrefilterManager.multi_line_specials = True

# Enable history saving
c.HistoryManager.enabled = True

# Set the history file path
c.HistoryManager.hist_file = '/opt/bluesky/ipython/profile_root/history.sqlite' 
