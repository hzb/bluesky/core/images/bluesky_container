# Bluesky container 

This repository is used to configure an environment for running bluesky using a docker container. 

## How to run the demo

1. Clone an instance of [vanilla beamlinetools](https://codebase.helmholtz.cloud/hzb/bluesky/beamlines_endstations/vanilla_beamline/source/beamlinetools) or use an existing one.

3. Create a directory to save the data in.
4. Create a location for user scripts 
5. Create a location for history persistance
6. run the following:

    You will need to replace the paths for the mounted volumes to the paths on your local machine. Note that the definition of the TILED environment variables is optional. You will need to change them for your deployment. After editing those variables add them to ~/.bashrc and open a new terminal or use `source ~/.bashrc`.


```
export TILED_IP=http://localhost:8000
export TILED_SECRET=secret
export BEAMLINETOOLS_PATH=/path/to/beamlinetools
export DATA_PATH=/path/to/persistence/data
export USERSCRIPTS_PATH=/path/to/userscripts
export HISTORY_PATH=/path/to/history

```

7. Use script `run_bluesky.sh` from [`bluesky_startup_scripts`](https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bluesky_startup_scripts/-/blob/main/run_bluesky.sh?ref_type=heads) to start the ipython shell, if you want to start the container without the ipython shell use `run_bluesky.sh sh`


#### Make a scan in bluesky using simulated devices ####

    RE(dscan([noisy_det], motor, -10,10,10))

or using the magics:

    %dscan [noisy_det] motor -1 1 10

# GitLab CI/CD Pipeline Documentation with Charts

This document provides an overview of the GitLab CI/CD pipeline defined in the `.gitlab-ci.yml` file. The pipeline is designed for building, testing, and analyzing Docker images, with various stages tailored for different branches, tags, and development processes.

---

## **Pipeline Stages Overview**

The pipeline consists of the following stages:

| Stage                | Description                                                                                   |
|----------------------|-----------------------------------------------------------------------------------------------|
| **build_branch**     | Creates a tarball of the branch image.                                                       |
| **branch_scan**      | Scans the tarball with Trivy for vulnerabilities.                                             |
| **check_branch_image**| Verifies if the branch image is built correctly.                                             |
| **build_develop**    | Builds the Docker image when the merge request targets the development branch.               |
| **test_latest**      | Tests the Docker image tagged as `latest`.                                                   |
| **build_tag**        | Builds the Docker image when a tag is pushed.                                                |
| **test_tag**         | Tests the Docker image for a pushed tag.                                                     |
| **test_branch**      | Runs tests for branch-specific Docker images.                                                |
| **analyze_branch**   | Analyzes test reports for branch-specific builds.                                             |
| **test_develop**     | Tests the Docker image for the development branch.                                           |
| **analyze_develop**  | Analyzes test reports for the development branch.                                             |
| **tests_tag**        | Tests the Docker image for a tag.                                                             |
| **analyze_tag**      | Analyzes test reports for a tag.                                                              |

---

## **Pipeline Workflow**

The following chart illustrates the pipeline workflow and its stages:

```mermaid
graph TD
  build_branch --> branch_scan --> check_branch_image
  check_branch_image -->|On Merge Request| build_develop
  build_develop --> test_develop --> analyze_develop
  build_branch -->|On Tag| build_tag --> test_tag --> analyze_tag
  build_branch --> test_branch --> analyze_branch
  build_tag --> tests_tag --> analyze_tag
  test_latest --> analyze_branch
```

---

## **Pipeline Configuration**

### Global Settings
- **Image**: `registry.hzdr.de/hzb/it-automation/ubuntu_22_04_docker:latest`
- **Services**: `docker:dind` (Docker in Docker)
- **Variables**:
  - `DOCKER_HOST`: `tcp://docker:2375/`
  - `DOCKER_DRIVER`: `overlay2`
  - `DOCKER_TLS_CERTDIR`: ""

### Stage Descriptions

#### **test_branch**
- Installs necessary tools (Python, pip, Docker Compose, etc.).
- Clones the `beamlinetools` repository and sets up tests.
- Loads the Docker image tarball and verifies container state.
- Executes Bluesky-related tests and collects artifacts.

#### **analyze_branch**
- Parses test reports using Python scripts.
- Stores analysis results as artifacts.

#### **test_develop**
- Similar to `test_branch` but specifically targets the development branch.

#### **analyze_develop**
- Analyzes test reports for the development branch.

#### **tests_tag**
- Similar to `test_branch` but triggers on tagged commits.

#### **analyze_tag**
- Analyzes test reports for tagged commits.

---

## **Artifacts**
- **Paths**: Reports (`report.xml`) and test outputs.
- **Retention**: Artifacts are stored for all test and analysis stages.

---

## **Pipeline Inclusion**
- Includes additional configuration from the `hzb/docker-image-ci` repository:
  ```yaml
  include:
    - project: 'hzb/docker-image-ci'
      ref: main
      file: '.gitlab-ci.yml'
  ```

---

## **Key Rules**

| Stage          | Trigger Rules                                                                                     |
|----------------|---------------------------------------------------------------------------------------------------|
| **test_branch**| Triggered on changes to any file in the branch. Not triggered for merge requests to `develop`.    |
| **analyze_branch**| Triggered on changes to any file in the branch.                                               |
| **test_develop**| Always triggered for the `develop` branch.                                                      |
| **analyze_develop**| Always triggered for the `develop` branch.                                                   |
| **tests_tag**   | Only triggered on tags.                                                                         |
| **analyze_tag** | Only triggered on tags.                                                                         |

---
.


